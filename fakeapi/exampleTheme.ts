export enum Trad {
  One = 1,
  Two = 2,
  Three = 3,
  Four = 4
}

export function ThemeRandomizer() {
  const themes = {
    exampleThemeOne: {
      backgroundColor: "#000000",
      backgroundImageUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      logoUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/180d5f0237148d777de10569e24bfae6-grayLogo.svg",
      faviconUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5d537df9692dba4e6d13769f3fb6da5d-favicon.ico",
      bannerUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      description: "Meta descricao da plataforma",
      keywords: "meta;keywords;",
      _id: "60edd080e3f7402cf8ef4189",
      platformId: "102a35f7-eeca-40ee-b35f-1505c901943f",
      platformName: "Platform F",
      primaryColor: "#7f7f7f",
      secondaryColor: "#a7dd5f",
      tertiaryColor: "#2e2d2d",
      textColor: "#fff",
      inputColor: "#333333",
      backgroundType: "image",
      backgroundGradientFirst: null,
      backgroundGradientSecond: null,
      backgroundGradientDegree: null,
      borderRadius: 0,
      title: "Titulo da Plataforma",
      footer: "Agradecimentos plataforma Xgrow.",
      supportNumber: "",
      supportEmail: "",
      createdAt: "2021-07-13T17:42:24.268Z",
      updatedAt: "2021-07-23T06:31:25.544Z",
      __v: 3,
    },
    exampleThemeTwo:  {
      backgroundColor: "#3D3D3D",
      backgroundImageUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      logoUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/180d5f0237148d777de10569e24bfae6-grayLogo.svg",
      faviconUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5d537df9692dba4e6d13769f3fb6da5d-favicon.ico",
      bannerUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      description: "Meta descricao da plataforma",
      keywords: "meta;keywords;",
      _id: "60edd080e3f7402cf8ef4189",
      platformId: "102a35f7-eeca-40ee-b35f-1505c901943f",
      platformName: "Platform F",
      primaryColor: "#FFF28C",
      secondaryColor: "#FF0005",
      tertiaryColor: "#959595",
      textColor: "#FF0000",
      inputColor: "#959595",
      backgroundType: "image",
      backgroundGradientFirst: null,
      backgroundGradientSecond: null,
      backgroundGradientDegree: null,
      borderRadius: 0,
      title: "Revolution",
      footer: "#gray #grey #pale #red #revolt #smoke #yellow",
      supportNumber: "",
      supportEmail: "",
      createdAt: "2021-07-13T17:42:24.268Z",
      updatedAt: "2021-07-23T06:31:25.544Z",
      __v: 3
    },
    exampleThemeThree: {
      backgroundColor: "#2A4359",
      backgroundImageUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      logoUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/180d5f0237148d777de10569e24bfae6-grayLogo.svg",
      faviconUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5d537df9692dba4e6d13769f3fb6da5d-favicon.ico",
      bannerUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      description: "Meta descricao da plataforma",
      keywords: "meta;keywords;",
      _id: "60edd080e3f7402cf8ef4189",
      platformId: "102a35f7-eeca-40ee-b35f-1505c901943f",
      platformName: "Platform F",
      primaryColor: "#FFF28C",
      secondaryColor: "#166B8C",
      tertiaryColor: "#AAB1BF",
      textColor: "#4E84A6",
      inputColor: "#2A4359",
      backgroundType: "image",
      backgroundGradientFirst: null,
      backgroundGradientSecond: null,
      backgroundGradientDegree: null,
      borderRadius: 0,
      title: "Denisa Marielena Tudose",
      footer: "Denisa Marielena Tudose",
      supportNumber: "",
      supportEmail: "",
      createdAt: "2021-07-13T17:42:24.268Z",
      updatedAt: "2021-07-23T06:31:25.544Z",
      __v: 3
    },
    exampleThemeFour: {
      backgroundColor: "#023559",
      backgroundImageUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      logoUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/180d5f0237148d777de10569e24bfae6-grayLogo.svg",
      faviconUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5d537df9692dba4e6d13769f3fb6da5d-favicon.ico",
      bannerUrl:
        "https://la-xgrow.sfo3.digitaloceanspaces.com/5772df9ecc46ffa3928bafcf8b70a735-MicrosoftTeams-image%20%282%29.png",
      description: "Meta descricao da plataforma",
      keywords: "meta;keywords;",
      _id: "60edd080e3f7402cf8ef4189",
      platformId: "102a35f7-eeca-40ee-b35f-1505c901943f",
      platformName: "Platform F",
      primaryColor: "#971115",
      secondaryColor: "#D92024",
      tertiaryColor: "#60C6C5",
      textColor: "#FFB700",
      inputColor: "#2A4359",
      backgroundType: "image",
      backgroundGradientFirst: null,
      backgroundGradientSecond: null,
      backgroundGradientDegree: null,
      borderRadius: 0,
      title: "Алмаз Хабибутдинов",
      footer: "Алмаз Хабибутдинов",
      supportNumber: "",
      supportEmail: "",
      createdAt: "2021-07-13T17:42:24.268Z",
      updatedAt: "2021-07-23T06:31:25.544Z",
      __v: 3
    }
  };

  


  function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  const getRandomTheme = () => {
    const randomNumber = randomIntFromInterval(1, 4)
    return themes[`exampleTheme${Trad[randomNumber]}`]
  }
  
  return {
    getRandomTheme
  }
}