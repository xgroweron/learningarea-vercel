import { ICredentials } from "types/credentials";
import { ThemeRandomizer } from './exampleTheme';
import { exampleUI } from './exampleUI';

export function fakeApi() {
  const getTheme = (platformId: string) =>
    new Promise((resolve, reject) => {
      console.log('getting theme for platformId:', platformId)
      const randomizer = ThemeRandomizer()
      setTimeout(() => {
        resolve(randomizer.getRandomTheme());
      }, 100);
    });

  const getContent = () =>
    new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          teste: "oi",
        });
      }, 500);
    });
  
  const getUX = () =>
    new Promise((resolve, reject) => {
      const standardPlatform = '102a35f7-eeca-40ee-b35f-1505c901943f'
      const UX = exampleUI(standardPlatform)
      setTimeout(() => {
        resolve({ui: UX});
      }, 100);
    })

  const login = (credentials: ICredentials) =>
    new Promise((resolve, reject) => {
      if (
        credentials.emailValue == 'teste@xgrow.com' &&
        credentials.passwordValue === '123xgrow'
      ) {
        setTimeout(() => {
          localStorage[`token-${credentials.platformId}`] = "12345"
          resolve({
            token: "12345",
          });
        }, 2000);
      } else {
        setTimeout(() => {
          return reject({ error: 'teste' })
        }, 100)
      }
    });

  const tokenVerify = (platformId: string) =>
    new Promise((resolve, reject) => {
      const currentToken = localStorage[`token-${platformId}`]
      if (!currentToken) return reject()
      setTimeout(() => {
        resolve({
          status: 200,
        });
      })
    })

  const getClients = (): Promise<string[]> =>
    new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve([
          '102a35f7-eeca-40ee-b35f-1505c901943f',
          '123',
          'oie'
        ])
      }, 100);
    })


  return {
    getTheme,
    getContent,
    getUX,
    login,
    tokenVerify,
    getClients
  };
}
