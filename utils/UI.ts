import { IWidget, IHeader, IPage } from 'types/UI';

export class ExampleUI {
  private _Page: IPage
  private _header: IHeader
  private _widgetList: IWidget[]

  constructor(
    private Page: IPage,
    private header: IHeader,
    private widgetList: IWidget[]
  ) {
    this._Page = Page;
    this._header = header;
    this._widgetList = widgetList;
  }

  public getUi() {
    return {
      page: this._Page,
      header: this._header,
      widgetList: this._widgetList,
    }
  }

  public getPage() {
    return {
      id: this._Page.id,
      hasHeader: this._Page.hasHeader,
      title: this._Page.title,
      createdDate: this._Page.createdDate,
    }
  }

  public getHeader() {
    return this._header;
  }

  public getWidgetList() {
    return this._widgetList;
  }

  public setPage(props: IPage) {
    Object.entries(props).forEach(e => this._Page[e[0]] = props[e[0]])
  }

  public setHeader(props: IHeader) {
    Object.entries(props).forEach(e => this._header[e[0]] = props[e[0]])
  }

  public setWidgetList(props: IWidget[]) {
    Object.entries(props).forEach(e => this._widgetList = props)
  }
}