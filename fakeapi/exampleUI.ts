import { ExampleUI } from 'utils/UI';
import { IWidget, IHeader, WidgetTypes, Colors, Carousel } from 'types/UI';

export function exampleUI(id) {
    //use REM for height
    //https://nekocalc.com/px-to-rem-converter
    const header: IHeader = {
        type: 1,
        heightDesktop: 24,
        heightMobile: 10,
        scrollHideDesktop: true,
        scrollHideMobile: true,
        mobileDisplayLogo: true,
        mobileSidebarAnimation: false
    }

    const pageInfo = {
        id: id,
        hasHeader: true,
        title: "Título da página",
        createdDate: "01/01/2022"
    }

    function carouselOne(): IWidget {
        const data: Carousel = {
            content: [
                {
                    img: 'https://res.cloudinary.com/dwvrok1le/image/upload/v1539362375/z63youp272k5mbqbuqj9.jpg',
                    text: 'Cynthia',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/1/10/Season_2_-_Marie.jpg/revision/latest?cb=20120617211645',
                    text: 'Marie Schrader',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/3/38/Bolsa.png/revision/latest?cb=20180825204033',
                    text: 'Juan Bolsa',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://res.cloudinary.com/dwvrok1le/image/upload/v1540314304/c8acek3pimb0hb4efrvf.jpg',
                    text: 'Christian Ortega',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/b/b7/HankS5.jpg/revision/latest/scale-to-width-down/700?cb=20120620014136',
                    text: 'Henry Schrader',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/9/95/Todd_brba5b.png/revision/latest?cb=20130717134303',
                    text: 'Todd Alquist',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/5/5e/Gretchen_Schwartz.png/revision/latest?cb=20131005103735&path-prefix=es',
                    text: 'Gretchen Schwartz',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/e/ec/Tomas.png/revision/latest?cb=20130131043014',
                    text: 'Tomás Cantillo',
                    link: 'https://breakingbadapi.com/'
                },
                {
                    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/1/1f/BCS_S4_Gustavo_Fring.jpg/revision/latest?cb=20180824195925',
                    text: 'Gustavo Fring',
                    link: 'https://breakingbadapi.com/'
                },
            ]
        }
        return {
            widgetType: WidgetTypes.HorizontalCarousel,
            mobileHeight: 300,
            desktopHeight: 500,
            title: 'Carrossel Horizontal',
            customBackground: Colors.Primary,
            data: data
        }
    }

    const UI = new ExampleUI(pageInfo, header, [carouselOne()])
    return UI.getUi()
}