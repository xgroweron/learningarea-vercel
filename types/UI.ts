export enum SidebarAnimation {
  SlideFromLeft
}

export enum SandwichPosition {
  Left = "left",
  Right = "right"
}

export enum Colors {
  Primary,
  Secondary
}

export enum WidgetTypes {
  ActionButton = 'heuae',
  HorizontalCarousel = 'HorizontalCarousel',
  VerticalCarousel = 'VerticalCarousel',
}

interface CarouselContent {
    img: string
    text: string
    link: string
}
export interface Carousel {
  settings?: {
    dots: boolean
    infinite: boolean
    speed: number
    slidesToShow: number
    autoplay: boolean
    slidesToScroll: number
    autoplaySpeed: number
  },
  content: CarouselContent[]
}


export interface IHeader {
  type?: number
  heightDesktop?: number
  heightMobile?: number
  scrollHideDesktop?: boolean
  scrollHideMobile?: boolean
  mobileSandwichPosition?: SandwichPosition | "right"
  mobileDisplayLogo?: boolean
  mobileSidebarAnimation?: SidebarAnimation | false
}

export interface IWidget {
  widgetType: WidgetTypes
  title: string
  mobileHeight?: number
  desktopHeight?: number
  customBackground?: Colors | false
  data: any
}

export interface IPage {
  id?: number
  hasHeader?: IHeader | boolean
  title?: string
  createdDate?: string
}