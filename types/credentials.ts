export interface ICredentials {
    emailValue: string;
    passwordValue: string;
    platformId: string;
}