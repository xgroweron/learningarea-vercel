import type { NextPage } from "next";
import { useState, useCallback, useEffect } from "react";
import Header from "components/Header";
import { fakeApi } from "fakeapi";
import { IStyle } from "types/theme";
import Widget from "components/Widget";
export async function getStaticPaths(props) {
  const apiCall = fakeApi();
  const clients = await apiCall.getClients();

  return {
    paths: clients.map(sp => ({
      params: {
        platformId: sp
      }
    })
    ),
    fallback: false
  };
}

export async function getStaticProps(context) {
  const { platformId } = context.params
  const apiCall = fakeApi()
  const themeAndClientInfo = await apiCall.getTheme(platformId);
  const uiFromFigma = await apiCall.getUX();
  return {
    props: {
      style: themeAndClientInfo,
      UI: uiFromFigma
    },
  };
}
interface ILearningArea {
  style: IStyle
  platformId: string
  UI: any
}

const LearningArea: NextPage<ILearningArea> = function ({ style, UI }) {
  const { ui } = UI
  const { backgroundColor } = style
  
  useEffect(() => {
    console.log('ui', ui)
  }, [])

  useEffect(() => {
    
  }, [])

  return (
    <>
      <main
        className="leading-normal tracking-normal h-screen"
        style={{backgroundColor}}
      >
        <div className="h-full">
          <Header style={style} ui={UI}/>
          {/* Main */}
          <div className="container pt-2 md:pt-5 mx-auto flex flex-wrap flex-col md:flex-row items-center">
            {ui.widgetList.length && ui.widgetList.map((e, i)=> (
              <Widget style={style} ui={UI} key={i} element={e}/>
            ))}
          </div>
        </div>
      </main>
    </>
  );
};

export default LearningArea;
