import "../styles/globals.css";
import "tailwindcss/tailwind.css";
import type { AppProps } from "next/app";
import { useRouter } from 'next/router'
import { routes } from 'routes';
import { useEffect } from "react";
import dashToCamel from "utils/dashToCamel";

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  useEffect(() => {
    const platformId = router.query.platformId;
    const currentRoute = dashToCamel(router.route.split('/')[2]);
    const token = localStorage[`token-${platformId}`];
    if (routes[currentRoute]?.isSafe && routes[currentRoute].name !== 'login' && routes[currentRoute].name !== '404') {
      if (!token) router.push('/'+platformId+'/')
    }
  }, [])
  return (
    <>
      <Component {...pageProps} />
    </>
  );
}
export default MyApp;

