export const routes = {
    home: {
        name: 'home',
        route: '/',
        title: 'title home',
        meta: 'meta',
        isSafe: true,
    },
    learningArea: {
        name: 'learningArea',
        route: '/learning-area',
        title: 'Learning Area',
        meta: 'meta',
        isSafe: true,
    },
    login: {
        name: 'login',
        route: '/',
        title: 'title login',
        meta: 'meta',
        isSafe: false,
    },
    404: {
        name: 'notfound',
        route: '/404',
        title: 'title 404',
        meta: 'meta',
        isSafe: false,
    },
}