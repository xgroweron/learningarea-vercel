import type { NextPage } from "next";
import { useState, useCallback, useEffect } from "react";
import { fakeApi } from "fakeapi";
import { IStyle } from "types/theme";
import { Input } from 'types/inputLoginTypes';
import { useRouter } from 'next/router'

export async function getStaticPaths(props) {
  const apiCall = fakeApi();
  const clients = await apiCall.getClients();

  return {
    paths: clients.map(sp => ({
      params: {
        platformId: sp
      }
    })
    ),
    fallback: false
  };
}

export async function getStaticProps({ params: { platformId } }) {
  const apiCall = fakeApi();
  const res = await apiCall.getTheme(platformId);
  return {
    props: {
      style: res,
      platformId: platformId
    },
  };
}


interface ILogin {
  style: IStyle
  platformId: string
}

const Login: NextPage<ILogin> = function ({style}) {
  const router = useRouter()
  const [passwordRecovery, setPasswordRecovery] = useState<boolean>(false);
  const [passwordShow, setPasswordShow] = useState<boolean>(false);
  const [emailValue, setEmailValue] = useState<string>("");
  const [passwordValue, setPasswordValue] = useState<string>("");
  const [recoveryEmailAddress, setRecoveryEmailAddress] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false)
  const [errorLogin, setErrorLogin] = useState<boolean>(false)
  const [errorRecover, setErrorRecover] = useState<boolean>(false)
  const [recoverSent, setRecoverSent] = useState<boolean>(false)
  const [slideEmailIcons, setSlideEmailIcons] = useState<boolean>(false)
  const [slidePasswordIcons, setSlidePasswordIcons] = useState<boolean>(false)
  const [showBackground, setShowBackground] = useState<boolean>(false)
  const [remember, setRemember] = useState<boolean>(true)
  const {
    backgroundColor,
    backgroundGradientDegree,
    backgroundGradientFirst,
    backgroundGradientSecond,
    backgroundImageUrl,
    backgroundType,
    bannerUrl,
    borderRadius,
    createdAt,
    description,
    faviconUrl,
    footer,
    inputColor,
    keywords,
    logoUrl,
    platformId,
    platformName,
    primaryColor,
    secondaryColor,
    supportEmail,
    supportNumber,
    tertiaryColor,
    textColor,
    title,
    updatedAt,
  } = style;

  useEffect(() => { setErrorLogin(false) }, [emailValue, passwordValue, recoveryEmailAddress])

  // credentials.email !== 'teste@xgrow.com' || 
  // credentials.password !== '123xgrow'


  const handleLogin = useCallback(async () => {
    setLoading(true)
    try {
      const apiCall = fakeApi();
      await apiCall.login({ emailValue, passwordValue, platformId })
      router.push(platformId + '/learning-area')
    } catch (e) {
      setErrorLogin(true)
    } finally {
      setLoading(false)
    }
  }, [emailValue, passwordValue])

  const handleRecover = useCallback(() => {
    if (!recoveryEmailAddress) return setErrorRecover(true)
    setLoading(true)
    setTimeout(() => {
      setRecoverSent(true)
      setLoading(false)
    }, 1000);
  }, [recoveryEmailAddress])

  const handleFocusInput = (status, input) => {
    if (status && input === Input.email) return setSlideEmailIcons(true)
    if (!status && input === Input.email) return setSlideEmailIcons(false)
    if (status && input === Input.password) return setSlidePasswordIcons(true)
    if (!status && input === Input.password) return setSlidePasswordIcons(false)
  }

  useEffect(() => {
    window.innerWidth > 780 ? setShowBackground(true) : '';
  }, [])

  return (
    <>
      <main
        className="leading-normal tracking-normal h-screen"
        style={{
          backgroundColor,
          backgroundImage: showBackground ? `url(${backgroundImageUrl})` : '',
          backgroundSize: 'auto',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center'
        }}>
        <div className="h-full" >
          <h3 className="text-gray-500"></h3>
          {/* Main */}
          <div className="container md:pt-24 md:pt-36 mx-auto flex flex-wrap flex-col md:flex-row items-center md:h-full flex-col" >
            {/* Left Col */}
            <div className="w-full xl:w-3/5 p-12 overflow-hidden flex justify-center">
                          
            </div>

            {/* Right Col */}
            <div className="flex flex-col w-full xl:w-1/4 justify-center lg:items-start overflow-y-hidden">
              {!passwordRecovery && (
                <form className="bg-gray-900 opacity-75 w-full shadow-lg md:rounded-lg px-7 xl:px-8 pt-6 pb-8 mb-4">
                  <div className="mb-4">
                    <div className="css-4302v8">
                      <p
                        className="opacity-0 flex flex-row justify-center pt-5 font-boldtext-2xl mb-8"
                        style={{ color: textColor }}
                      >
                        Recuperação de Senha
                      </p>
                      <div
                        className="css-j7bzn4"
                        style={{
                          position: "relative",
                          top: "20px",
                          left: slideEmailIcons ? '2px' : '10px',
                          transition: '0.4s',
                          zIndex: "1",
                          width: 0,
                          height: 0,
                        }}
                      >
                        <svg
                          stroke={primaryColor}
                          fill={tertiaryColor}
                          strokeWidth="0"
                          viewBox="0 0 24 24"
                          focusable="false"
                          className=""
                          height="1em"
                          width="1em"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{
                            transform: `scale(${slideEmailIcons ? '1.3' : '1'})`,
                            transition: '0.4s'
                          }}
                        >
                          <path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"></path>
                        </svg>
                      </div>
                      <input
                        className="shadow appearance-none border rounded w-full p-4 pl-10 text-gray-500 font-bold leading-tight focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
                        id="emailaddress"
                        type="text"
                        placeholder="Email"
                        value={emailValue}
                        onChange={(e) => setEmailValue(e.target.value)}
                        required
                        onMouseOver={() => handleFocusInput(true, Input.email)}
                        onMouseOut={() => handleFocusInput(false, Input.email)}
                      />
                    </div>
                  </div>
                  <div className="mb-4">
                    <div className="css-a0kk5w">
                      <div
                        className="css-j7bzn4"
                        style={{
                          position: "relative",
                          top: "20px",
                          left: slidePasswordIcons ? '0px' : '10px',
                          transition: '0.4s',
                          zIndex: "1",
                          width: 0,
                          height: 0,
                        }}
                      >
                        <svg
                          stroke="currentColor"
                          fill={tertiaryColor}
                          strokeWidth="0"
                          viewBox="0 0 24 24"
                          focusable="false"
                          className="chakra-icon css-13otjrl"
                          height="1em"
                          width="1em"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{
                            transform: `scale(${slidePasswordIcons ? '1.3' : '1'})`,
                            transition: '0.4s'
                          }}
                        >
                          <path d="M18 8h-1V6c0-2.76-2.24-5-5-5S7 3.24 7 6v2H6c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V10c0-1.1-.9-2-2-2zm-6 9c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2zm3.1-9H8.9V6c0-1.71 1.39-3.1 3.1-3.1 1.71 0 3.1 1.39 3.1 3.1v2z"></path>
                        </svg>
                      </div>
                      <div>
                        <input
                          className="shadow appearance-none border rounded w-full p-4 pl-10 text-gray-500 font-bold leading-tight focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
                          id="password"
                          type={passwordShow ? 'text' : 'password'}
                          placeholder="Senha"
                          value={passwordValue}
                          onChange={(e) => setPasswordValue(e.target.value)}
                          onMouseOver={() => handleFocusInput(true, Input.password)}
                          onMouseOut={() => handleFocusInput(false, Input.password)}
                        />
                        <div
                          className="css-iakt80"
                          style={{
                            position: "relative",
                            top: "-35px",
                            left: `calc(100% - ${slidePasswordIcons ? "20px" : "35px"})`,
                            transition: '0.4s',
                            zIndex: "1",
                            width: 0,
                            height: 0,
                          }}
                        >
                          <svg
                            onClick={() => setPasswordShow(!passwordShow)}
                            stroke="currentColor"
                            fill={tertiaryColor}
                            strokeWidth="0"
                            viewBox="0 0 24 24"
                            focusable="false"
                            className="cursor-pointer css-11p1vtb"
                            height="1em"
                            width="1em"
                            xmlns="http://www.w3.org/2000/svg"
                            style={{
                              transform: `scale(${slidePasswordIcons ? '1.3' : '1'})`,
                              transition: '0.4s'
                            }}
                          >
                            {passwordShow ? (
                              <path d="M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z"></path>
                            ) : (
                              <path d="M12 7c2.76 0 5 2.24 5 5 0 .65-.13 1.26-.36 1.83l2.92 2.92c1.51-1.26 2.7-2.89 3.43-4.75-1.73-4.39-6-7.5-11-7.5-1.4 0-2.74.25-3.98.7l2.16 2.16C10.74 7.13 11.35 7 12 7zM2 4.27l2.28 2.28.46.46C3.08 8.3 1.78 10.02 1 12c1.73 4.39 6 7.5 11 7.5 1.55 0 3.03-.3 4.38-.84l.42.42L19.73 22 21 20.73 3.27 3 2 4.27zM7.53 9.8l1.55 1.55c-.05.21-.08.43-.08.65 0 1.66 1.34 3 3 3 .22 0 .44-.03.65-.08l1.55 1.55c-.67.33-1.41.53-2.2.53-2.76 0-5-2.24-5-5 0-.79.2-1.53.53-2.2zm4.31-.78l3.15 3.15.02-.16c0-1.66-1.34-3-3-3l-.17.01z"></path>
                            )}
                          </svg>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="flex flex-row justify-between">
                    <div className="flex align-center">
                      <input
                        type="checkbox"
                        style={{
                          width: "20px",
                          height: "20px",
                        }}
                        name="remember"
                        defaultChecked={remember}
                        onClick={() => setRemember(!remember)}
                      />
                      <label
                        htmlFor="remember"
                        className="pl-2 text-sm"
                        style={{ color: textColor }}
                      >
                        Lembrar-me
                      </label>
                    </div>
                    <div>
                      <p
                        onClick={() => setPasswordRecovery(true)}
                        className="cursor-pointer text-sm"
                        style={{ color: textColor }}
                      >
                        Esqueceu sua senha?
                      </p>
                    </div>
                  </div>

                  <p className={`text-red-500 italic my-0 w-100 text-center ${!errorLogin && 'text-opacity-0'}`}>Login inválido!</p>
                  <div className="flex items-center justify-between pt-4">
                    <button
                      className="w-80 h-16 uppercase font-bold mx-auto py-2 px-4 rounded focus:ring transform transition hover:scale-105 duration-300 ease-in-out flex items-center justify-center"
                      type="button"
                      style={{ background: primaryColor, color: textColor }}
                      onClick={handleLogin}
                    >
                      {loading && <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                        <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
                        <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                      </svg>}
                      Entrar
                    </button>
                  </div>
                  <p
                    className="flex flex-row justify-center pt-5"
                    style={{ color: textColor }}
                  >
                    Precisa de ajuda?
                    <a
                      href="malito:test@xgrow.com"
                      className="pl-2 font-bold"
                      style={{ color: primaryColor }}
                    >
                      Fale com o suporte
                    </a>
                  </p>
                </form>
              )}
              {passwordRecovery && (
                <form className="bg-gray-900 opacity-75 w-full shadow-lg rounded-lg px-8 pt-6 pb-8 mb-4 h-5/6">
                  <p
                    className="flex flex-row justify-center pt-5 font-bold text-2xl mb-8"
                    style={{ color: textColor }}
                  >
                    Recuperação de Senha
                  </p>
                  <p
                    className="flex flex-row justify-center font-bold text-base text-center mb-10"
                    style={{ color: textColor }}
                  >
                    Digite seu email, você receberá um link para redefinir sua
                    senha.
                  </p>
                  <div className="mb-4">
                    <div className="css-4302v8">
                      <div
                        className="css-j7bzn4"
                        style={{
                          position: "relative",
                          top: "20px",
                          left: slidePasswordIcons ? '0px' : '10px',
                          transition: '0.4s',
                          zIndex: "1",
                          width: 0,
                          height: 0,
                        }}
                      >
                        <svg
                          stroke={primaryColor}
                          fill={tertiaryColor}
                          strokeWidth="0"
                          viewBox="0 0 24 24"
                          focusable="false"
                          className=""
                          height="1em"
                          width="1em"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{
                            transform: `scale(${slidePasswordIcons ? '1.3' : '1'})`,
                            transition: '0.4s'
                          }}
                        >
                          <path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"></path>
                        </svg>
                      </div>
                      <input
                        className="shadow appearance-none border rounded w-full p-4 pl-10 font-bold leading-tight focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
                        id="recoveryEmaiAddress"
                        type="text"
                        placeholder="Email"
                        required
                        value={recoveryEmailAddress}
                        onChange={(e) => setRecoveryEmailAddress(e.target.value)}
                        onMouseOver={() => handleFocusInput(true, Input.password)}
                        onMouseOut={() => handleFocusInput(false, Input.password)}
                      />
                    </div>
                  </div>
                  <p className={`text-red-500 italic my-0 w-100 text-center ${!errorRecover && 'text-opacity-0'} ${recoverSent && 'h-0'}`}>Email inválido!</p>
                  <p className={`italic my-0 w-100 text-center text-white ${!recoverSent && 'h-0 text-opacity-0'} `}
                  >Favor verificar sua caixa de email.</p>
                  <div className="flex items-center justify-between pt-4">
                    <button
                      className="w-80 h-16 uppercase font-bold py-2 px-4 mx-auto rounded focus:ring transform transition hover:scale-105 duration-300 ease-in-out flex items-center justify-center"
                      type="button"
                      style={{ background: primaryColor, color: textColor }}
                      onClick={handleRecover}
                    >
                      {loading && <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                        <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
                        <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                      </svg>}

                      Recuperar Acesso
                    </button>
                  </div>
                  <div className="flex flex-row justify-center pt-5 text-white">
                    <p
                      onClick={() => setPasswordRecovery(false)}
                      className="pl-2 font-bold cursor-pointer"
                      style={{ color: primaryColor }}
                    >
                      Voltar para Login
                    </p>
                  </div>
                </form>
              )}
            </div>

            {/* Footer */}
            <div className="w-full pt-8 md:pt-16 md:pb-6 text-sm text-center md:text-left fade-in">
              <a
                rel="norereferrer"
                className="text-gray-500 no-underline hover:no-underline mr-1"
                href="#"
                style={{ color: primaryColor }}
              >
                &copy; {title}
              </a>
              <span style={{ color: textColor }}>- Criado por</span>
              <a
                rel="norereferrer"
                className="text-gray-500 no-underline hover:no-underline ml-1"
                href="https://www.xgrow.com"
                style={{ color: primaryColor }}
              >
                XGrow
              </a>
              <p style={{ color: primaryColor }}>{footer}</p>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Login;
