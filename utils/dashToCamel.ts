export default function dashToCamel(camel) {
    if (!camel) return ''
    return camel.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
}