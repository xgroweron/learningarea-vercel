import { css } from '@emotion/css';

export const widgetBase = ({backgroundColor,
    backgroundGradientDegree,
    backgroundGradientFirst,
    backgroundGradientSecond,
    backgroundImageUrl,
    backgroundType,
    bannerUrl,
    borderRadius,
    createdAt,
    description,
    faviconUrl,
    footer,
    inputColor,
    keywords,
    logoUrl,
    platformId,
    platformName,
    primaryColor,
    secondaryColor,
    supportEmail,
    supportNumber,
    tertiaryColor,
    textColor,
    title,
    updatedAt}) =>
  css`
    color: ${textColor};
  `