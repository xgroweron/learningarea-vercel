import { useEffect, FunctionComponent } from "react";
import { IWidget } from 'types/UI'
import { IStyle } from 'types/theme'
import Carousel from 'components/Carousel'
interface IUI {
  ui?: {
    widgets: IWidget[]
  }
}

interface IHeaderComponent {
  style: IStyle
  platformId?: string
  ui?: IUI
  element: any
}

const Header: FunctionComponent<IHeaderComponent> = function ({ style, ui, element }) {
  const {
    backgroundColor,
    backgroundGradientDegree,
    backgroundGradientFirst,
    backgroundGradientSecond,
    backgroundImageUrl,
    backgroundType,
    bannerUrl,
    borderRadius,
    createdAt,
    description,
    faviconUrl,
    footer,
    inputColor,
    keywords,
    logoUrl,
    platformId,
    platformName,
    primaryColor,
    secondaryColor,
    supportEmail,
    supportNumber,
    tertiaryColor,
    textColor,
    title,
    updatedAt } = style

  useEffect(() => {
    console.log('eron', ui)
    console.log('element', element)
  }, [])

  return (
    <>
      {
        element.widgetType == "VerticalCarousel" && (
          <>
            <Carousel style={style} element={element}/>
          </>
        )
      }
      {
        element.widgetType == "HorizontalCarousel" && (
          <>
            <Carousel style={style} element={element}/>
          </>
        )
      }
    </>
  );
};

export default Header;
