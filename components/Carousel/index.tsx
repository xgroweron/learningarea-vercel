import { useState, useEffect, FunctionComponent } from "react";
import { IWidget } from 'types/UI'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { IStyle } from 'types/theme'

interface IUI {
  ui?: {
    widgets: IWidget
  }
}

interface ICarouselComponent {
  style: IStyle
  platformId?: string
  ui?: IUI
  element: any
}

const Carousel: FunctionComponent<ICarouselComponent> = function ({ style, ui, element }) {
  const [settings, setSettings] = useState({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    autoplay: false,
    slidesToScroll: 1,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })
  const [isMobile, setIsMobile] = useState(true)
  const {
    backgroundColor,
    backgroundGradientDegree,
    backgroundGradientFirst,
    backgroundGradientSecond,
    backgroundImageUrl,
    backgroundType,
    bannerUrl,
    borderRadius,
    createdAt,
    description,
    faviconUrl,
    footer,
    inputColor,
    keywords,
    logoUrl,
    platformId,
    platformName,
    primaryColor,
    secondaryColor,
    supportEmail,
    supportNumber,
    tertiaryColor,
    textColor,
    title,
    updatedAt } = style

  useEffect(() => {
    window.innerWidth > 1024 ? (setSettings({
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      autoplay: false,
      slidesToScroll: 1,
      autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }), setIsMobile(false)) : '';
  }, [])

  return (
    <div style={{ width: "100%" }}>
      <Slider {...settings} className="test">
        {element.data.content.length && element.data.content.map((e, i) => {
          return (
            <a key={i} href={e.link} target="_blank" style={{display: 'block', }} rel="noreferrer">
              <img src={e.img} className="w-full object-cover h-72 p-2" alt={e.text}/>
              <h3 style={{ margin: 'auto', textAlign: 'center', color: textColor }}>{e.text}</h3>
          </a>)
        })}
      </Slider>
    </div>
  );
};

export default Carousel;
